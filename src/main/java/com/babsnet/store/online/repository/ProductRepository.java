package com.babsnet.store.online.repository;

import com.babsnet.store.online.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    Page<Product> getAllByAndActiveIsTrue(Pageable pageable);
    Product getByIdAndActiveIsTrue(Long id);
}
