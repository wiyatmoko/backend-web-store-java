package com.babsnet.store.online.model;

import lombok.*;


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequestFilter {
    private Integer page;
    private Integer size;
    private String sortBy;
    private String direction;
    private String searchQuery;
    private Long categoryId;
}

