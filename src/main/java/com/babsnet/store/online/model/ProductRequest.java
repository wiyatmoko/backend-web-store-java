package com.babsnet.store.online.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class ProductRequest {
    private String sku;
    private String name;
    private String description;
    private boolean active;
    private int unitsInStock;
    private BigDecimal unitPrice;
    private Long categoryId;
    private List<MultipartFile> file;
}
