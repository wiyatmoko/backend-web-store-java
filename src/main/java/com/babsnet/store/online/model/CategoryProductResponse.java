package com.babsnet.store.online.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryProductResponse {
    private Long id;
    private String name;
}
