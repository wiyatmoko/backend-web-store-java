package com.babsnet.store.online.spesification;

import com.babsnet.store.online.entity.Product;
import com.babsnet.store.online.entity.ProductCategory;
import com.babsnet.store.online.model.ProductRequestFilter;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class ProductSpesification implements Specification<Product> {
    private final ProductRequestFilter filter;

    public ProductSpesification(ProductRequestFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (filter != null && filter.getSearchQuery() != null && !filter.getSearchQuery().isEmpty()) {
            String searchQuery = "%" + filter.getSearchQuery().toLowerCase() + "%";
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), searchQuery),
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), searchQuery)
            ));
        }

        if (filter != null && filter.getCategoryId() != null) {
            Join<Product, ProductCategory> categoryJoin = root.join("category", JoinType.INNER);
            predicates.add(criteriaBuilder.equal(categoryJoin.get("id"), filter.getCategoryId()));
        }

        predicates.add(criteriaBuilder.isTrue(root.get("active")));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
