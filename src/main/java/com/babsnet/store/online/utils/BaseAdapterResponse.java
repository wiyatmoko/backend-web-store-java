package com.babsnet.store.online.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseAdapterResponse<T> {
    private String status;
    private Integer code;
    private T data;
    private String message;
    private Object pagination;

    public static <T> BaseAdapterResponse<T> ok(T data) {
        BaseAdapterResponse<T> response = new BaseAdapterResponse<T>();
        response.code = HttpStatus.OK.value();
        response.message = "success";
        response.status = "" + HttpStatus.OK;
        response.data = data;
        response.pagination = new HashMap<>();
        return response;
    }
    public static <T> BaseAdapterResponse<T> ok(T data, Long total) {
        BaseAdapterResponse<T> response = new BaseAdapterResponse<T>();
        response.code = HttpStatus.OK.value();
        response.message = "success";
        response.status = "" + HttpStatus.OK;
        response.data = data;
        Map<String, Object> pageMap = new HashMap<>();
        pageMap.put("total", total);
        response.pagination = pageMap;
        return response;
    }

    public static BaseAdapterResponse error(String data) {
        BaseAdapterResponse  response = new BaseAdapterResponse();
        response.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        response.message = data;
        response.status = "" + HttpStatus.INTERNAL_SERVER_ERROR;
        return response;
    }
}