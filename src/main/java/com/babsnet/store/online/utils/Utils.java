package com.babsnet.store.online.utils;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

public class Utils {
    public static BigDecimal convertStringToBigDecimal(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("String cannot be empty or null");
        }
        try {
            return new BigDecimal(str);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid BigDecimal format");
        }
    }

    public static boolean convertIntToBoolean(int value) {
        if (value == 1) {
            return true;
        } else if (value == 0) {
            return false;
        } else {
            throw new IllegalArgumentException("Invalid integer value. Expected 0 or 1.");
        }
    }

    public static MultipartFile compressImage(MultipartFile file) throws IOException {
        // Read uploaded image file
        BufferedImage originalImage = ImageIO.read(file.getInputStream());

        // Compress image and store in byte array
        ByteArrayOutputStream compressedImageStream = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", compressedImageStream);
        byte[] compressedImageData = compressedImageStream.toByteArray();

        // Convert byte array to MultipartFile
        return convertToMultipartFile(compressedImageData, file.getOriginalFilename());
    }

    private static MultipartFile convertToMultipartFile(byte[] fileBytes, String fileName) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(fileBytes);
        return new MockMultipartFile("file", fileName, "image/jpeg", inputStream);
    }
}
