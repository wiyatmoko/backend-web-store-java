package com.babsnet.store.online.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Service
public class UploadImage {

    private static AmazonS3 s3;


    static String imageUrl;

    @Autowired
    public UploadImage(AmazonS3 s3) {
        this.s3 = s3;
    }

    public static String save(String path,
                              String fileName,
                              Optional<Map<String, String>> optionalMetadata,
                              InputStream inputStream) {
        ObjectMetadata metadata = new ObjectMetadata();
        optionalMetadata.ifPresent(map -> {
            if(!map.isEmpty()) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (key == "Content-Length") {
                        metadata.setContentLength(Long.parseLong(value));
                    }
                    if (key == "Content-Type") {
                        metadata.setContentType(value);
                    }
                }
            }
        });
        try {
            s3.putObject(new PutObjectRequest(path, fileName, inputStream, metadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
            imageUrl = s3.getUrl(path,fileName).toString();
        } catch (AmazonServiceException e) {
            throw new IllegalStateException("Failed to store file", e);
        }

        return imageUrl;
    }
}
