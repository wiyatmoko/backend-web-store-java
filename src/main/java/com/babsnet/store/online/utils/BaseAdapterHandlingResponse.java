package com.babsnet.store.online.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseAdapterHandlingResponse {
    private String status;
    private Integer code;
    private String message;


    public static BaseAdapterHandlingResponse nok(HttpStatus httpStatus, Exception exception, int code) {
        BaseAdapterHandlingResponse response = new BaseAdapterHandlingResponse();
        response.code = code;
        response.message = exception.getMessage();
        response.status = "" + httpStatus;
        return response;
    }

}
