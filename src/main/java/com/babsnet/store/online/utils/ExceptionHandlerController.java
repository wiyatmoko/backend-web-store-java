package com.babsnet.store.online.utils;

import com.babsnet.store.online.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@Slf4j
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { AlreadyExistException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<Object> handleAlreadyExistException(
            AlreadyExistException ex, WebRequest request) {
        int code = 30001;
        BaseAdapterHandlingResponse bodyOfResponse = BaseAdapterHandlingResponse.nok(HttpStatus.CONFLICT, ex, code);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = { NotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleNotFoundException(
            NotFoundException ex, WebRequest request) {
        int code = 30000;
        BaseAdapterHandlingResponse bodyOfResponse = BaseAdapterHandlingResponse.nok(HttpStatus.NOT_FOUND, ex, code);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { InvalidValueFieldException.class })
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<Object> handleInvalidValueFieldException(
            InvalidValueFieldException ex, WebRequest request) {
        int code = 30002;
        BaseAdapterHandlingResponse bodyOfResponse = BaseAdapterHandlingResponse.nok(HttpStatus.UNPROCESSABLE_ENTITY, ex, code);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ExceptionHandler(value = { AnauthorizedException.class })
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<Object> handleUnauthorizedException(
            AnauthorizedException ex, WebRequest request) {
        int code = 80001;
        BaseAdapterHandlingResponse bodyOfResponse = BaseAdapterHandlingResponse.nok(HttpStatus.UNAUTHORIZED, ex, code);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = { ErrorServerException.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> handleAllException(
            Exception ex, WebRequest request) {
        int code = 80000;
        BaseAdapterHandlingResponse bodyOfResponse = BaseAdapterHandlingResponse.nok(HttpStatus.INTERNAL_SERVER_ERROR, ex, code);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
