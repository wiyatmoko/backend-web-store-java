package com.babsnet.store.online.controller;

import ch.qos.logback.classic.Logger;
import com.babsnet.store.online.entity.ProductCategory;
import com.babsnet.store.online.model.CategoryProductResponse;
import com.babsnet.store.online.service.CategoryProductService;
import com.babsnet.store.online.utils.BaseAdapterResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/category")
@RequiredArgsConstructor
public class CategoryProductController {

    private final CategoryProductService categoryProductService;
    private static final Logger logger = (Logger) LoggerFactory.getLogger(CategoryProductController.class);
    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseAdapterResponse<List<?>> getAll(@RequestParam(value = "page", defaultValue = "0") Integer pageNo,
                                               @RequestParam(value = "size", defaultValue = "10") Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        logger.debug("Success get data category  : {}" , "");
        Page<ProductCategory> productCategories =  categoryProductService.getAll(pageable);
        List<ProductCategory> productCategoryList = productCategories.getContent();
        return BaseAdapterResponse.ok(productCategoryList.stream().map(productCategory -> CategoryProductResponse.builder()
                .id(productCategory.getId())
                .name(productCategory.getCategoryName())
                .build()).collect(Collectors.toList()), productCategories.getTotalElements());
    }
}
