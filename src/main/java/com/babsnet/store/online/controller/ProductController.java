package com.babsnet.store.online.controller;

import ch.qos.logback.classic.Logger;
import com.babsnet.store.online.entity.Product;
import com.babsnet.store.online.model.ProductRequest;
import com.babsnet.store.online.model.ProductRequestFilter;
import com.babsnet.store.online.model.ProductResponse;
import com.babsnet.store.online.service.ProductService;
import com.babsnet.store.online.utils.BaseAdapterResponse;
import com.babsnet.store.online.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProductController.class);

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseAdapterResponse<List<?>> getAll(@RequestParam(value = "page", defaultValue = "0") Integer pageNo,
                                               @RequestParam(value = "size", defaultValue = "10") Integer pageSize,
                                               @RequestParam(value = "sort_direction", defaultValue = "DESC") String direction,
                                               @RequestParam(value = "sortBy", defaultValue = "dateCreated") String sortBy,
                                               @RequestParam(value = "searchQuery", defaultValue = "") String searchQuery,
                                               @RequestParam(value = "categoryId", defaultValue = "") Long categoryId) {
        ProductRequestFilter filter = ProductRequestFilter.builder().build();
        filter.setPage(pageNo);
        filter.setSize(pageSize);
        filter.setDirection(direction);
        filter.setSortBy(sortBy);
        filter.setSearchQuery(searchQuery);
        filter.setCategoryId(categoryId);
        Page<Product> productPage= productService.getAll(filter);
        List<Product> productList = productPage.getContent();
        logger.debug("Success get data product  : {}" , productList);
        return BaseAdapterResponse.ok(productList.stream().map(product -> ProductResponse.builder()
                .productId(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getUnitPrice())
                .stock(Long.valueOf(product.getUnitsInStock()))
                .imageUrl(product.getProductImageUrls())
                .build()).collect(Collectors.toList()), productPage.getTotalElements());
    }


    @SneakyThrows
    @PostMapping(path = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseAdapterResponse<?> add(@RequestPart("files") List<MultipartFile> files,
                                      @RequestParam("sku") String sku,
                                      @RequestParam("name") String name,
                                      @RequestParam("description") String description,
                                      @RequestParam("status") String active,
                                      @RequestParam("stock") String stock,
                                      @RequestParam("price") String price,
                                      @RequestParam("categoryId") String categoryId) {
        ProductRequest request = ProductRequest.builder().build();
        request.setSku(sku);
        request.setName(name);
        request.setDescription(description);
        request.setFile(files);
        request.setCategoryId(Long.valueOf(categoryId));
        request.setActive(Utils.convertIntToBoolean(Integer.parseInt(active)));
        request.setUnitsInStock(Integer.parseInt(stock));
        request.setUnitPrice(Utils.convertStringToBigDecimal(price));
        List<MultipartFile> multipartFileList = new ArrayList<>();
        request.getFile().forEach(f-> {
            try {
                logger.debug("Compress image size: {}", f.getSize());
                multipartFileList.add(Utils.compressImage(f));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        for (MultipartFile f : multipartFileList){
            logger.debug("Name: {}", f.getName());
            logger.debug("size: {}", f.getSize());
        }
        ProductResponse productResponse = productService.add(request, multipartFileList);
        return BaseAdapterResponse.ok(productResponse);
    }

    @SneakyThrows
    @GetMapping("/{id}")
    public BaseAdapterResponse<?> get(@PathVariable String id){
        ProductResponse productResponse = productService.getById(id);
        return BaseAdapterResponse.ok(productResponse);
    }
}
