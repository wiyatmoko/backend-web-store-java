package com.babsnet.store.online.service;

import com.babsnet.store.online.entity.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryProductService {
    Page<ProductCategory> getAll(Pageable pageable);
}
