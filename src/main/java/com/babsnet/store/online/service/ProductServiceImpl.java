package com.babsnet.store.online.service;

import ch.qos.logback.classic.Logger;
import com.babsnet.store.online.entity.Product;
import com.babsnet.store.online.entity.ProductCategory;
import com.babsnet.store.online.entity.ProductImage;
import com.babsnet.store.online.exception.NotFoundException;
import com.babsnet.store.online.model.ProductRequest;
import com.babsnet.store.online.model.ProductRequestFilter;
import com.babsnet.store.online.model.ProductResponse;
import com.babsnet.store.online.repository.ProductCategoryRepository;
import com.babsnet.store.online.repository.ProductImageRepository;
import com.babsnet.store.online.repository.ProductRepository;
import com.babsnet.store.online.spesification.ProductSpesification;
import com.babsnet.store.online.utils.UploadImage;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService{
    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProductServiceImpl.class);
    @Autowired
    private final ProductRepository productRepository;

    private final ProductCategoryRepository productCategoryRepository;

    private final ProductImageRepository productImageRepository;
    @Override
    public Page<Product> getAll(ProductRequestFilter filter) {
        return productRepository.findAll(new ProductSpesification(filter),  PageRequest.of(filter.getPage(), filter.getSize(), Sort.Direction.valueOf(filter.getDirection()), filter.getSortBy()));
    }

    @Override
    @SneakyThrows
    public ProductResponse add(ProductRequest request, List<MultipartFile> multipartFileList) {
        Optional<ProductCategory> productCategoryOptional = productCategoryRepository.findById(request.getCategoryId());
        ProductCategory productCategory = productCategoryOptional.orElseThrow(() -> new NotFoundException("ProductCategory is not available" + request.getCategoryId()));
        LocalDateTime now = LocalDateTime.now();

        Product productNew = Product.builder()
                        .active(request.isActive())
                        .sku(request.getSku())
                        .dateCreated(now)
                        .unitPrice(request.getUnitPrice())
                        .unitsInStock(request.getUnitsInStock())
                        .category(productCategory)
                        .name(request.getName())
                        .description(request.getDescription())
                .build();
        Product product = productRepository.save(createProductImage(multipartFileList, productNew));
        return ProductResponse.builder()
                .productId(product.getId())
                .stock(Long.valueOf(product.getUnitsInStock()))
                .price(product.getUnitPrice())
                .name(product.getName())
                .description(product.getDescription())
                .imageUrl(product.getProductImageUrls())
                .build();
    }

    @Override
    public ProductResponse getById(String id) {
        Product product = productRepository.getByIdAndActiveIsTrue(Long.valueOf(id));
        return ProductResponse.builder()
                .productId(product.getId())
                .stock(Long.valueOf(product.getUnitsInStock()))
                .price(product.getUnitPrice())
                .name(product.getName())
                .description(product.getDescription())
                .imageUrl(product.getProductImageUrls())
                .build();
    }

    private Product createProductImage(List<MultipartFile> multipartFileList, Product productNew) {
        List<ProductImage> productImageList = new ArrayList<>();
        multipartFileList.forEach(file->{
            String linkUrl = upload(file);
            productImageList.add(ProductImage.builder()
                            .product(productNew)
                            .imageUrl(linkUrl)
                    .build());
            if(linkUrl !=null || !linkUrl.isEmpty()){
                deleteImageFile(file);
            }
        });
        productNew.setProductImage(productImageList);
        return productNew;
    }


    @SneakyThrows
    public String upload(MultipartFile file) {
        String imageUrl;
        //check if the file is not empty
        if (file.isEmpty()) {
            throw new NotFoundException("No file added");
        }

        //prepare metadata
        Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-Type", file.getContentType());
        metadata.put("Content-Length", String.valueOf(file.getSize()));
        //store the file
        //create a path depending on the username, so that all of a user's files are in the same directory
        String path = String.format("%s/%s", "gms-s3-storage-aws", "online-store");
        //create a filename from original filename and random UUID
        //String filename = String.format("%s-%s", file.getOriginalFilename(), UUID.randomUUID());
        try {
            imageUrl  = UploadImage.save(path, file.getOriginalFilename(), Optional.of(metadata), file.getInputStream());
        } catch (IOException e) {
            throw new IllegalStateException("error", e);
        }
        logger.debug("ImageUrl: {}", imageUrl);
        return imageUrl;
    }


    // Method to delete image file
    public void deleteImageFile(MultipartFile file) {
        File imageFile = new File(file.getOriginalFilename());
        if(imageFile.exists()) {
            if(imageFile.delete()) {
                logger.info("File deleted");
            } else {
                logger.info("File deletion failed.");
            }
        } else {
            logger.info("File does not exist.");
        }
    }
}
