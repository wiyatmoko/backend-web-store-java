package com.babsnet.store.online.service;

import com.babsnet.store.online.entity.ProductCategory;
import com.babsnet.store.online.repository.ProductCategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductCategoryServiceImpl implements CategoryProductService {

    private final ProductCategoryRepository productCategoryRepository;
    @Override
    public Page<ProductCategory> getAll(Pageable pageable) {
        return productCategoryRepository.findAll(pageable);
    }
}
