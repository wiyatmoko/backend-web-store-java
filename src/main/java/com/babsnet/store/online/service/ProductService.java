package com.babsnet.store.online.service;

import com.babsnet.store.online.entity.Product;
import com.babsnet.store.online.model.ProductRequest;
import com.babsnet.store.online.model.ProductRequestFilter;
import com.babsnet.store.online.model.ProductResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ProductService {
    Page<Product> getAll(ProductRequestFilter pageable);

    ProductResponse add(ProductRequest request, List<MultipartFile> multipartFileList);

    ProductResponse getById(String id);
}
