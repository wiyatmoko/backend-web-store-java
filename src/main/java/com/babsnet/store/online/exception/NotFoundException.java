package com.babsnet.store.online.exception;

public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
}