package com.babsnet.store.online.exception;

public class InvalidValueFieldException extends Exception {
    public  InvalidValueFieldException(String message) {
        super(message);
    }
}
