package com.babsnet.store.online.exception;

public class AnauthorizedException extends  Exception{
    public AnauthorizedException(String message) {
        super(message);
    }
}
