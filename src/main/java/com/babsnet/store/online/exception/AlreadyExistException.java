package com.babsnet.store.online.exception;

public class AlreadyExistException extends Exception{
    public AlreadyExistException(String message) {
        super(message);
    }
}
