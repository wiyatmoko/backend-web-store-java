package com.babsnet.store.online.exception;

public class ErrorServerException extends Exception{
    public  ErrorServerException(String message) {
        super(message);
    }
}
